
## SIG组名称
Sysmonitor

本sig组负责规范安全类应用/平台与操作系统间网络安全监测信息的数据描述，形成统一安全类监管数据通信交互标准，并提供跨平台（x86/ARM/RISC-V）API接口，推动相关工具规划、开发和维护。

## 工作目标

1. 规范安全类应用/平台与操作系统间通信接口及格式，形成安全监测数据交互规范；
1. 接口标准化，兼容能源、交通、金融、应急、环保、水利等行业应用标准；
1. 提供系统API接口，实现网络安全监测数据采集和安全策略等交互功能接口；
1. 兼容多款CPU架构，如x86、ARM、RISC-V等。

## SIG成员

SIG-owner
- (杨诏钧`yangzhaojun@kylinos.cn`)

SIG-maintainer
- (张大朋`zhangdapeng@kylinos.cn`)
- (张楠`zhangnan@kylinos.cn`)


## SIG维护包列表

- ky-sysmonitor


## SIG邮件列表
> sysmonitor@lists.openkylin.top

## SIG组例会
> 双周例会