## Server adaption SIG小组

Server adaption SIG provides support for openKylin's kernel and software adaptation on server platforms.

## Goal-setting

1. Integrate server technology resources and user ecological needs to accelerate the development of openKylin in the field of server operating systems.
2. Prosper the openKylin server operating system software ecosystem and accelerate the resolution of common problems.
3. Joint sharing and cooperative development evolution of related software tools.

## SIG Member

## Maintainers
Jian Gu (`gujian@phytium.com.cn`)
Hongbo Mao (`maohongbo@phytium.com.cn`)
Jiakun Shuai (`shuaijiakun1288@phytium.com.cn`)

## SIG Maintenance Packages List

## Mailing List
server@lists.openkylin.top
