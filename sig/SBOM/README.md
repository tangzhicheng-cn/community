## SBOM SIG

本sig组负责推动和促进软件物料清单（SBOM）的发展和工具建设。

## 工作目标
- **SBOM工具建设**
- **推广和普及SBOM概念**
- **提供SBOM实施指南**


## SIG成员

### Owner

- 罗婷婷（jdt-devops）
- 邱佰惠（rea-seven）

### Maintainers

- 王军（okaywang888）
- 田凯（tk103331）
- 张朝琦（sevaae）
- 张振宇（zhangzhenyu_1_0）
- 刘坤（js-xxk）

## 邮件列表

