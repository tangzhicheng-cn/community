# AIFramework兴趣小组（SIG）

AIFramework SIG致力于为openKylin社区适配流行的AI框架，以DEB的包格式提供给开发者。

## 工作目标

- 满足爱好者在openkylin中方便快捷的安装智能计算框架，减少适配成本。

## 维护包列表

- [cloud-client-ai-service-framework](https://gitee.com/openkylin/cloud-client-ai-service-framework)
- [paddlepaddle](https://gitee.com/openkylin/paddlepaddle)

## SIG 成员

### Owner
- 商晓阳(15063566195@163.com)
### Maintainers
- 戴博奇(daiboqi@kylinos.cn)
- 张渊(zhangyuan@kylinos.cn)
- 周迪思(zhoudisi@kylinos.cn)
- 丁晨光(dingchenguang@kylinos.cn)

## 邮件列表

- [aiframework@lists.openkylin.top](mailto:aiframework@lists.openkylin.top)
